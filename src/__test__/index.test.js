import "@testing-library/jest-dom"
import {render, screen} from "@testing-library/react";
import Home from "../../pages";

test('should show welcome ', () => {
    render(<Home/>)
    const tag = screen.getByRole('contentinfo')
    expect(tag).toBeInTheDocument()
})